import os

from flask import Flask, request
from flask_assets import Environment
from flask_compress import Compress
from flask_login import LoginManager
from flask_mail import Mail
from flask_rq import RQ
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect

from app.assets import app_css, app_js, vendor_css, vendor_js
from config import config as Config

from flask_uploads import UploadSet, configure_uploads, IMAGES, AUDIO
from werkzeug.utils import secure_filename
from flask_ckeditor import CKEditor
#from app.utils import get_cart, image_size, json_load



basedir = os.path.abspath(os.path.dirname(__file__))

#UPLOAD_FOLDER = '/home/ubuntu/FlaskApp/flask-base/app/static/'
UPLOAD_FOLDER = basedir+'/static/'
VIDEO_FOLDER = basedir+'/static/videos'

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'mp4'}

mail = Mail()
db = SQLAlchemy()
csrf = CSRFProtect()
compress = Compress()
images = UploadSet('images', IMAGES)
media = UploadSet('media')

# Set up Flask-Login
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'account.login'


def create_app(config):
    app = Flask(__name__)
    config_name = config

    if not isinstance(config, str):
        config_name = os.getenv('FLASK_CONFIG', 'default')

    app.config.from_object(Config[config_name])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    # not using sqlalchemy event system, hence disabling it
    app.config['UPLOADS_DEFAULT_DEST'] = UPLOAD_FOLDER
    app.config['UPLOADED_FILES_DEST'] = VIDEO_FOLDER
    app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

    Config[config_name].init_app(app)

    # Set up extensions
    mail.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    csrf.init_app(app)
    compress.init_app(app)
    RQ(app)
    configure_uploads(app, (images, media))
    ckeditor = CKEditor(app)

    # Register Jinja template functions
    from .utils import register_template_utils
    register_template_utils(app)

    # Set up asset pipeline
    assets_env = Environment(app)
    dirs = ['assets/styles', 'assets/scripts']
    for path in dirs:
        assets_env.append_path(os.path.join(basedir, path))
    assets_env.url_expire = True

    assets_env.register('app_css', app_css)
    assets_env.register('app_js', app_js)
    assets_env.register('vendor_css', vendor_css)
    assets_env.register('vendor_js', vendor_js)

    # Configure SSL if platform supports it
    if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        from flask_sslify import SSLify
        SSLify(app)

    # Create app blueprints

    from .blueprints.page import page as page_blueprint
    app.register_blueprint(page_blueprint)

    from .blueprints.event import event as event_blueprint
    app.register_blueprint(event_blueprint)
    
    from .blueprints.programme import programme as programme_blueprint
    app.register_blueprint(programme_blueprint)
    
    from .blueprints.section import section as section_blueprint
    app.register_blueprint(section_blueprint)

    from .blueprints.public import public as public_blueprint
    app.register_blueprint(public_blueprint)

    from .blueprints.organisation import organisation as organisation_blueprint
    app.register_blueprint(organisation_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .blueprints.account import account as account_blueprint
    app.register_blueprint(account_blueprint, url_prefix='/account')

    from .blueprints.admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .blueprints.blog import blog
    app.register_blueprint(blog, url_prefix='/blog')

    #from .blueprints.marketplace import marketplace as marketplace_blueprint
    #app.register_blueprint(marketplace_blueprint, url_prefix='/marketplace')


    return app
