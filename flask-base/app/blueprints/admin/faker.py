import json

from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from flask_uploads import UploadSet, IMAGES
from flask_wtf.file import FileAllowed
from wtforms import Flags

from app import db
from app.blueprints.admin.forms import MCategoryForm, MCurrencyForm, MShippingMethodForm, MProductForm
from app.blueprints.admin.views import admin
from app.decorators import admin_required
from app.models import *

images = UploadSet('images', IMAGES)

from sqlalchemy.exc import IntegrityError
from random import seed, choice
from faker import Faker
fake = Faker()
seed()


@admin.route('/faker', methods=['GET', 'POST'])
@login_required
@admin_required
def add_fake():

    if request.method == 'POST':
        org = Organisation(
    org_name=fake.company(),
    org_city=fake.city(),
    org_state=fake.last_name(),
    org_country=fake.country(),
    org_website=fake.email(),
    org_short_description=fake.text(max_nb_chars=80),
    org_description=fake.text(),
    logos=Logo.query.first(),
    user=User.query.first())
        db.session.add(org)
        print("passed 2")
        s = Section(
    section_name=fake.word(),
    section_title=fake.first_name(),
    section_description=fake.text(),
    image=Logo.id,
    organisation_id=Organisation.query.first(),
    owner_organisation=Organisation.org_name)
        db.session.add(s)
        print("passed 3")        
        try:
            db.session.commit()
            print("passed 1")
            flash('Data added', 'form-success')
        except IntegrityError:
            db.session.rollback()
    else:
        return ""

