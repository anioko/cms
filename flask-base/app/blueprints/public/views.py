from flask import Blueprint, render_template, redirect, url_for, flash, abort
from flask_login import login_required, current_user
from app.models import *
from .forms import *
import commonmark
from app import db
from app.decorators import admin_required
import datetime
from time import strftime,gmtime
from sqlalchemy import Date, cast
from datetime import date

public = Blueprint('public', __name__)


@public.route('/live')
def live():
    event = Event.query.first()
    banner = Banner.query.first()
    audio = Video.query.first()
    event_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    today = datetime.datetime.now()
    event_day = (db.session.query(Event)
      .filter(cast(Event.start_time, Date) == date.today())
      .first())
    #if event.start_time != event_time:
        #event = event.segment       
    return render_template('public/bootstrapmade.com/dewi/live.html', event=event,
                           banner=banner, video=audio, event_time=event_time,
                           today=today.strftime("%x"), event_day=event_day)


@public.route('/stream')
def stream():     
    return render_template('public/stream.html')

@public.route('/ecommerce')
def ecommerce():
    #organisation = Organisation.query.first()
    logo = Logo.query.first()
    #appname = AppName.query.first()
    #service = Service.query.all()
    #marketing = Marketing.query.first()
    #testiomonialsection = Testimonialsection.query.get(1)
    products = MProduct.query.filter_by(availability=True).filter_by(is_featured=True).all()
    categories_instances = MCategory.query.filter_by(is_featured=True).all()
    section = Section.query.all()
    return render_template('public/ecommerce/page-index-1.html', logo=logo, section=section,
                           categories=categories_instances, products=products)


@public.route('/show/', methods=['GET', 'POST'])
@login_required
def show_video():
    video = Video.query.first()
    banner = Banner.query.first()
    return render_template('programme/show.html', video=video, banner=banner)

@public.route('/dewi')
def dewi():
    #organisation = Organisation.query.first()
    logo = Logo.query.first()
    appname = AppName.query.first()
    event = Event.query.all()
    #service = Service.query.all()
    #marketing = Marketing.query.first()
    #testiomonialsection = Testimonialsection.query.get(1)
    section = Section.query.all()
    sections = Section.contents
    content = Content.query.all()
    org = Organisation.query.first()       
    return render_template('public/bootstrapmade.com/dewi/index.html', logo=logo, section=section,
                           appname=appname, event=event, content=content,
                           org=org, sections=sections)



@public.route('/starter', methods=['GET', 'POST'])
@public.route('/start', methods=['GET', 'POST'])
@public.route('/add/register', methods=['GET', 'POST'])
def admin_register():

    app_name_exist = AppName.query.get(1)
    if app_name_exist is not None:
        return redirect(url_for('account.add_branding'))

