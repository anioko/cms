from flask_ckeditor import CKEditorField
from flask_uploads import UploadSet, IMAGES
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    SelectField,
    IntegerField,
    DecimalField,
    FloatField,
    FileField,
    BooleanField,
    MultipleFileField,
    TextAreaField
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    Email,
    EqualTo,
    InputRequired,
    Length,
)
from wtforms.validators import Email, EqualTo, InputRequired, Length, Optional
from wtforms_alchemy import Unique, ModelForm, model_form_factory
from app.models import *


images = UploadSet('images', IMAGES)
BaseModelForm = model_form_factory(FlaskForm)


class PageForm(FlaskForm):
    name = StringField('Page Name e.g Use cases', validators=[InputRequired(), Length(1, 64)])
    title = StringField('One Line of Text e.g Making change happen one step at a time', [Length(max=180)])
    images = MultipleFileField('Product Images', validators=[InputRequired(), FileAllowed(images, 'Images only!')])
    description = TextAreaField('Description')

    is_onnavbar = BooleanField(u'Should this page show on the navigation bar?')
    is_frontpagevisible=BooleanField("Do you want to make this visible on the current landing page?")
    is_landingpage = BooleanField("Do you want to make this the landing (home) page for visitors ?")
    signin_required = BooleanField("Do you want visitors to sign in before viewing the page ?")
    
    google_analytics_id = StringField('Insert your Google Analytics ID here if tracking is needed')
    other_tracking_analytics_one = TextAreaField('Insert e.g Facebook Pixel Code here')
    other_tracking_analytics_two = TextAreaField('Insert e.g HotJar Analytics Code here')
    other_tracking_analytics_three = TextAreaField('Insert e.g HotJar Analytics Code here')
    other_tracking_analytics_four = TextAreaField('Insert e.g other Analytics Code here')

    html_code_one = TextAreaField('Insert e.g Html Code here')
    html_code_two = TextAreaField('Insert e.g Html Code here')
    html_code_three = TextAreaField('Insert e.g Html Code here')
    html_code_four = TextAreaField('Insert e.g Html Code here')

    submit = SubmitField('Submit')
