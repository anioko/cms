import json

from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from flask_uploads import UploadSet, IMAGES
from flask_wtf.file import FileAllowed
from wtforms import Flags

from app import db
from app.blueprints.page.forms import PageForm
from app.decorators import admin_required
from app.models import *
from flask import Blueprint, render_template

from app.models import EditableHTML

page = Blueprint('page', __name__)
images = UploadSet('images', IMAGES)

@page.route('/')
def index():
    return render_template('main/index.html')


@page.route('/about')
def about_page():
    editable_html_obj = EditableHTML.get_editable_html('about')
    return render_template(
        'main/about.html', editable_html_obj=editable_html_obj)


@page.route('/page/add', methods=['GET', 'POST'])
#@login_required
#@admin_required
def add_page():
    org = Organisation.query.get(1)
    form = PageForm()    
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filenames = []
            if request.files['images']:
                image_files = request.files.getlist('images')
                for image in image_files:
                    image_filename = images.save(image)
                    image_filenames.append(image_filename)
            image_filenames = json.dumps(image_filenames)
            page = Page(
                name=form.name.data,
                title=form.title.data,
                images=image_filenames,
                #owner_organisation=org.org_name,
                #organisation_id=org.id,
                description=form.description.data,
                html_code_one=form.html_code_one.data,
                html_code_two=form.html_code_two.data,
                html_code_three=form.html_code_three.data,
                html_code_four=form.html_code_four.data,
                google_analytics_id=form.google_analytics_id.data,
                other_tracking_analytics_one=form.other_tracking_analytics_one.data,
                other_tracking_analytics_two=form.other_tracking_analytics_two.data,
                other_tracking_analytics_three=form.other_tracking_analytics_three.data,
                other_tracking_analytics_four=form.other_tracking_analytics_four.data,
                is_onnavbar=form.is_onnavbar.data,
                is_frontpagevisible=form.is_frontpagevisible.data,
                is_landingpage=form.is_landingpage.data,
                signin_required=form.signin_required.data
            )

            db.session.add(page)
            db.session.commit()
            flash('Page {} successfully created'.format(page.name), 'success')
            return redirect(url_for('page.pages'))

    return render_template('page/add_page.html', form=form)



@page.route('/page/<int:page_id>/edit', methods=['GET', 'POST'])
#@login_required
#@admin_required
def edit_page(page_id):
    page = Page.query.get_or_404(page_id)
    
    form = PageForm(obj=page)
    
    if request.method == 'POST':
        if form.validate_on_submit():
            name = form.name.data,
            title = form.title.data,
            #images = form.images.data,
            description = form.description.data,
            html_code_one=form.html_code_one.data,
            html_code_two=form.html_code_two.data,
            html_code_three=form.html_code_three.data,
            html_code_four=form.html_code_four.data,
            google_analytics_id=form.google_analytics_id.data,
            other_tracking_analytics_one=form.other_tracking_analytics_one.data,
            other_tracking_analytics_two=form.other_tracking_analytics_two.data,
            other_tracking_analytics_three=form.other_tracking_analytics_three.data,
            other_tracking_analytics_four=form.other_tracking_analytics_four.data,
            is_onnavbar=form.is_onnavbar.data,
            is_frontpagevisible=form.is_frontpagevisible.data,
            is_landingpage=form.is_landingpage.data,
            is_landingpage=form.is_landingpage.data,
            signin_required=form.signin_required.data
            form.populate_obj(page)
            db.session.add(page)
            db.session.commit()

    return render_template('page/edit_page.html', form=form)

@page.route('/pages', defaults={'page': 1}, methods=['GET'])
@page.route('/pages/<int:page>', methods=['GET'])
#@login_required
#@admin_required
def pages(page):
    pages = Page.query.order_by(Page.created_at.asc()).paginate(page, per_page=50)
    pages_count = Page.query.count()
    return render_template('page/browse.html', pages=pages, pages_count=pages_count)

@page.route('/page/<int:page_id>/_delete', methods=['POST'])
#@login_required
#@admin_required
def delete_page(page_id):
    cat = Page.query.get_or_404(page_id)
    db.session.delete(cat)
    db.session.commit()
    flash('Successfully deleted Page.', 'success')
    return redirect(url_for('page.pages'))




