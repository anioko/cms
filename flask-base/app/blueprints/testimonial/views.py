import json

from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from flask_uploads import UploadSet, IMAGES
from flask_wtf.file import FileAllowed
from wtforms import Flags

from app import db
from app.blueprints.page.forms import PageForm
from app.decorators import admin_required
from app.models import *
from flask import Blueprint, render_template

from app.models import EditableHTML

testimonial = Blueprint('testimonial', __name__)
images = UploadSet('images', IMAGES)


@admin.route('/settings/testimonial/')
@login_required
@admin_required
def testimonial_dashboard():
    """Testimonial dashboard page."""
    return render_template('testimonial/testimonial_settings_dashboard.html')

@admin.route('/add/testimonial', methods=['Get', 'POST'])
@login_required
def add_testimonial():
    org = Organisation.query.get(1)
    form = TestimonialForm()

    if request.method == 'POST' and 'image' in request.files:
        image = images.save(request.files['image'])
        appt = Testimonial(image=image,
                    owner_organisation=org.org_name,
                    organisation_id=org.id,
                    person_name = form.person_name.data,
                    job_title = form.job_title.data,
                    description = form.description.data              
                          )
        db.session.add(appt)
        db.session.commit()
        flash('Testimonial added!', 'success')
        return redirect(url_for('testimonial.testimonials'))
    return render_template('testimonial/add_testimonial.html', form=form, org=org)


@admin.route('/<int:testimonial_id>/testimonial/edit', methods=['GET', 'POST'])
@login_required
def edit_testimonial(testimonial_id):
    org = Organisation.query.get(1)
    settings = Testimonial.query.filter_by(id=testimonial_id).first_or_404()
    photo = Testimonial.query.filter_by(id=testimonial_id).first_or_404()
    url = images.url(photo.image)
    form = TestimonialForm(obj=settings)
    if request.method == 'POST' and 'image' in request.files:
        image = images.save(request.files['image'])
        appt = Testimonial(image=image,
                    owner_organisation=org.org_name,
                    organisation_id=org.id,
                    person_name = form.person_name.data,
                    job_title = form.job_title.data,
                    description = form.description.data  
                          )
        db.session.add(appt)
        db.session.commit()
    #if request.method == 'POST' and 'image' in request.files:
       # image = images.save(request.files['image'])
        #form.populate_obj(settings)
        #db.session.add(settings)
        #db.session.commit()
        flash('Data edited!', 'success')
        return redirect(url_for('testimonial.testimonial_dashboard'))
    else:
        flash('Error! Data was not added.', 'error')
    return render_template('testimonial/edit_testimonial.html', form=form, url=url)


@admin.route('/testimonial', defaults={'page': 1}, methods=['GET'])
@admin.route('/testimonial/<int:page>', methods=['GET'])
@login_required
@admin_required
def testimonials(page):
    testimonials = Testimonial.query.paginate(page, per_page=100)
    return render_template('testimonial/browse.html', testimonials=testimonials)


@admin.route('/testimonial/<int:testimonial_id>/_delete', methods=['POST'])
@login_required
@admin_required
def delete_testimonial(testimonial_id):
    testimonial = Testimonial.query.filter_by(id=testimonial_id).first()
    db.session.delete(testimonial)
    db.session.commit()
    flash('Successfully deleted a testimonial member.', 'success')
    return redirect(url_for('testimonial.testimonials'))





