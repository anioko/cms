from flask_ckeditor import CKEditorField
from flask_uploads import UploadSet, IMAGES
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    SelectField,
    IntegerField,
    DecimalField,
    FloatField,
    FileField,
    BooleanField,
    MultipleFileField,
    TextAreaField
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    Email,
    EqualTo,
    InputRequired,
    Length,
)
from wtforms.validators import Email, EqualTo, InputRequired, Length, Optional
from wtforms_alchemy import Unique, ModelForm, model_form_factory
from app.models import *


images = UploadSet('images', IMAGES)
BaseModelForm = model_form_factory(FlaskForm)

class TestimonialForm(FlaskForm):
    person_name = StringField('Person name')
    job_title = StringField('Job title')
    description = StringField('Testimonial comment')
    image = FileField('Image', validators=[Optional(), FileAllowed(images, 'Images only!')])
    submit = SubmitField('Submit')
