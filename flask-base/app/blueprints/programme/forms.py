from flask_ckeditor import CKEditorField
from flask_uploads import UploadSet, IMAGES
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    SelectField,
    IntegerField,
    DecimalField,
    FloatField,
    FileField,
    BooleanField,
    MultipleFileField,
    TextAreaField
    
)
from wtforms.fields.html5 import EmailField, DateField, DateTimeLocalField
from wtforms.validators import (
    Email,
    EqualTo,
    InputRequired,
    Length,
)
from wtforms.validators import Email, EqualTo, InputRequired, Length, Optional
from wtforms_alchemy import Unique, ModelForm, model_form_factory
from app.models import *


images = UploadSet('images', IMAGES)
BaseModelForm = model_form_factory(FlaskForm)


class VideoForm(FlaskForm):
    video = FileField('Video', validators=[FileRequired()])
    submit = SubmitField('Submit')

class BannerForm(FlaskForm):
    banner = FileField('Banner Image', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    submit = SubmitField('Submit')

class ProgrammeForm(FlaskForm):
    segment = SelectField(u'Programme Segment', choices=[('Welcome', 'Welcome'), ('Opening', 'Opening'),
                                                         ('Praise', 'Praise'), ('Rhapsody Adults', 'Rhapsody Adults'),
                                                         ('Rhapsody Teens', 'Rhapsody Teens'), ('Rhapsody Kids', 'Rhapsody Kids'),
                                                         ('Promo', 'Promo'), ('Offering', 'Offering'),
                                                         ('Message', 'Message'), ('Closing', 'Closing')])
    title = StringField('Programme Title E.g a wonderful day with the Spirit')
    description = TextAreaField('Write anything people should see on the page or on search engines')
    start_time = DateTimeLocalField('StartDate & Time:', format='%Y-%m-%dT%H:%M')
    end_time = DateTimeLocalField('EndDate & Time:', format='%Y-%m-%dT%H:%M')
    video = FileField('Video', validators=[FileRequired()])
    image = FileField('Welcome banner Image to be displayed', validators=[Optional(), FileAllowed(images, 'Images only!')])
    banner_displayed = BooleanField("Display banner ?")
    image = FileField('Stay tuned banner Image to be displayed', validators=[Optional(), FileAllowed(images, 'Images only!')])
    stay_tuned_banner_displayed = BooleanField("Display stay tuned banner ?")
    login_required = BooleanField("Registration Required ?")

    submit = SubmitField('Submit')
