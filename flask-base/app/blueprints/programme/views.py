import json
import os
from flask import render_template, request, flash, redirect, url_for, current_app
from flask_login import login_required, current_user
from flask_uploads import UploadSet, IMAGES
from flask_wtf.file import FileAllowed
from werkzeug.utils import secure_filename
from wtforms import Flags

from app import db
from app.blueprints.programme.forms import *
from app.decorators import admin_required
from app.models import *

from flask import Blueprint, render_template

from app.models import EditableHTML

programme = Blueprint('programme', __name__)
images = UploadSet('images', IMAGES)
photos = UploadSet('photos', IMAGES)
media = UploadSet('photos', IMAGES)



#UPLOAD_FOLDER = '/home/ubuntu/FlaskApp/flask-base/app/static/'
#UPLOAD_FOLDER = '/Users/oem/Documents/bucket/ubuntu/FlaskApp/flask-base/app/static/videos'
UPLOAD_FILE=os.getcwd()+'/static/videos'

@programme.app_errorhandler(500)
def internal_server_error(_):
    return render_template('errors/500.html'), 500


@programme.route('/programme/add', methods=['Get', 'POST'])
@programme.route('/add/programme', methods=['Get', 'POST'])
#@login_required
def add_programme():
    form = ProgrammeForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = ""
            if request.files['image']:
                image_filename = images.save(request.files['image'])
            video_filename = request.files['video']
            if video_filename.filename != '':
            #if request.files['video']:
                video_filename.save(os.path.join(current_app.config['UPLOADED_FILES_DEST'], video_filename.filename))
     
            appt = Programme(banner=image_filename,
                segment = form.segment.data,
                video = video_filename.filename,
                description = form.description.data,
                start_time = form.start_time.data,
                end_time = form.end_time.data,
                banner_displayed = form.banner_displayed.data,   
                            )
            db.session.add(appt)
            db.session.commit()
            flash('programme {} successfully created'.format(appt.segment), 'success')
            return redirect(url_for('programme.programmes'))
    return render_template('programme/add_programme.html', form=form)


@programme.route('/<int:programme_id>/programme/edit', methods=['GET', 'POST'])
#@login_required
def edit_programme(programme_id):
    org = Organisation.query.get(1)
    settings = Programme.query.filter_by(id=programme_id).first_or_404()
    photo = Programme.query.filter_by(id=programme_id).first_or_404()
    banner = images.url(photo.banner)
    form = ProgrammeForm(obj=settings)
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = ""
            if request.files['image']:
                image_filename = images.save(request.files['image'])
            video_filename = request.files['video']
            if video_filename != '':
            #if request.files['video']:
                video_filename.save(video_filename)
     
            appt = Programme(banner=image_filename,
                segment = form.segment.data,
                video = video_filename,
                description = form.description.data,
                start_time = form.start_time.data,
                end_time = form.end_time.data,
                banner_displayed = form.banner_displayed.data,   
                            )
            form.populate_obj(appt)
            db.session.add(appt)
            db.session.commit()
    #if request.method == 'POST' and 'image' in request.files:
       # image = images.save(request.files['image'])
        #form.populate_obj(settings)
        #db.session.add(settings)
        #db.session.commit()
            flash('Data edited!', 'success')
            return redirect(url_for('programme.programmes'))
    else:
        flash('Error! Data was not added.', 'error')
    return render_template('programme/edit_programme.html', form=form, banner=banner)


@programme.route('/programme', defaults={'page': 1}, methods=['GET'])
@programme.route('/programme/<int:page>', methods=['GET'])
#@login_required
#@admin_required
def programmes(page):
    programmes = Programme.query.paginate(page, per_page=100)
    return render_template('programme/browse.html', programmes=programmes)


@programme.route('/programme/<int:programme_id>/_delete', methods=['GET'])
@login_required
@admin_required
def delete_programme(programme_id):
    programme = Programme.query.filter_by(id=programme_id).first()
    db.session.delete(programme)
    db.session.commit()
    flash('Successfully deleted a programme member.', 'success')
    return redirect(url_for('programme.programmes'))

@programme.route('/banner/upload', methods=['GET', 'POST'])
@login_required
def banner_upload():
   # ''' check if banner already exist, if it does, send to homepage. Avoid duplicate upload here'''
    #check_banner_exist = db.session.query(Banner).filter(Banner.organisation_id == Organisation.id).count()
    #if check_banner_exist >= 1:
        #return redirect(url_for('admin.church_dashboard'))
    form = BannerForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = images.save(request.files['banner'])
            image_url = images.url(image_filename)
            owner_organisation = db.session.query(Organisation).first()
            banner = Banner(
                image_filename=image_filename,
                image_url=image_url,
                owner_organisation=owner_organisation.org_name,
                organisation_id=owner_organisation.id
            )
            db.session.add(banner)
            db.session.commit()
            flash("Image saved.")
            return redirect(url_for('programme.banners'))
        else:
            flash('ERROR! Banner was not saved.', 'error')
    return render_template('programme/upload.html', form=form)


@programme.route('/banners', defaults={'page': 1}, methods=['GET'])
@programme.route('/banners/<int:page>', methods=['GET'])
@login_required
@admin_required
def banners(page):
    banners = Banner.query.paginate(page, per_page=100)
    banner = Banner.query.first()
    if banner is None:
        return redirect(url_for('programme.banner_upload'))

    return render_template('programme/banner_browse.html', banners=banners, banner=banner)

@programme.route('/banner/<int:banner_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_banner_upload(banner_id):
    banner = Banner.query.filter_by(id=banner_id).first_or_404()
    form = BannerForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = images.save(request.files['banner'])
            image_url = images.url(image_filename)
            owner_programme = db.session.query(Organisation).filter_by(user_id=current_user.id).first()
            banner.image_filename=image_filename,
            banner.image_url=image_url,
            banner.owner_programme=owner_organisation.org_name,
            banner.organisation_id=owner_organisation.id
            db.session.add(banner)
            db.session.commit()
            flash("Image saved.")
            return redirect(url_for('programme.banners'))
        else:
            flash('ERROR! Banner was not saved.', 'error')
    return render_template('programme/upload.html', form=form)

@programme.route('/banner/<int:banner_id>/_delete', methods=['GET'])
@login_required
@admin_required
def delete_banner(banner_id):
    banner = Banner.query.filter_by(id=banner_id).first()
    db.session.delete(banner)
    db.session.commit()
    flash('Successfully deleted Banner.', 'success')
    return redirect(url_for('admin.frontend_dashboard'))


@programme.route('/video/upload', methods=['GET', 'POST'])
@login_required
def video_upload():
    form = VideoForm()
    if form.validate_on_submit():
        f = form.video.data
        filename = secure_filename(f.filename)
        #f.save(filename)
        f.save(os.path.join(current_app.config['UPLOADED_FILES_DEST'], filename))
        video=Video(name=f.filename)
        db.session.add(video)
        db.session.commit()
        return 'file uploaded successfully'
    else:
        flash('ERROR! Banner was not saved.', 'error')
    return render_template('programme/video_upload.html', form=form)




##@programme.route('/programme/add', methods=['Get', 'POST'])
##@programme.route('/add/programme', methods=['Get', 'POST'])
###@login_required
##def add_programme():
##    org = Organisation.query.get(1)
##    form = programmeForm()
##
##    if request.method == 'POST' and 'image' in request.files:
##        image = images.save(request.files['image'])
##        appt = programme(image=image,
##                    #owner_programme=org.org_name,
##                    #programme_id=org.id,
##
##                    programme_name = form.programme_name.data,
##                    programme_title = form.programme_title.data,
##                    programme_description = form.programme_description.data
##                          )
##        db.session.add(appt)
##        db.session.commit()
##        flash('programme added!', 'success')
##        return redirect(url_for('programme.programmes'))
##    return render_template('programme/add_programme.html', form=form, org=org)
