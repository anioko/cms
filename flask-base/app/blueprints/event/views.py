import json
import os
from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from flask_uploads import UploadSet, IMAGES
from flask_wtf.file import FileAllowed
from wtforms import Flags

from app import db
from app.blueprints.event.forms import EventForm
from app.decorators import admin_required
from app.models import *

from flask import Blueprint, render_template

from app.models import EditableHTML

event = Blueprint('event', __name__)
images = UploadSet('images', IMAGES)
photos = UploadSet('photos', IMAGES)


@event.app_errorhandler(500)
def internal_server_error(_):
    return render_template('errors/500.html'), 500


@event.route('/event/add', methods=['Get', 'POST'])
@event.route('/add/event', methods=['Get', 'POST'])
#@login_required
def add_event():
    org = Organisation.query.get(1)
    form = EventForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = ""
            if request.files['image']:
                image_filename = images.save(request.files['image'])
            video_filename = request.files['video']
            if video_filename.filename != '':
            #if request.files['video']:
                video_filename.save(os.path.join(current_app.config['UPLOADED_FILES_DEST'], video_filename.filename))
     
            appt = Event(banner=image_filename,
                name = form.name.data,
                title = form.title.data,
                video = video_filename.filename,
                description = form.description.data,
                start_time = form.start_time.data,
                end_time = form.end_time.data,
                banner_displayed = form.banner_displayed.data,   
                login_required = form.login_required.data
                            )
            db.session.add(appt)
            db.session.commit()
            flash('event {} successfully created'.format(appt.name), 'success')
            return redirect(url_for('event.events'))
    return render_template('event/add_event.html', form=form)


@event.route('/<int:event_id>/event/edit', methods=['GET', 'POST'])
#@login_required
def edit_event(event_id):
    org = Organisation.query.get(1)
    settings = Event.query.filter_by(id=event_id).first_or_404()
    photo = Event.query.filter_by(id=event_id).first_or_404()
    url = images.url(photo.image)
    form = EventForm(obj=settings)
    if request.method == 'POST' and 'image' in request.files:
        image = images.save(request.files['image'])
        appt = Event(image=image,
                      #owner_organisation=org.org_name,
                      #organisation_id=org.id,
                    event_name = form.name.data,
                    event_title = form.title.data,
                    event_description = form.description.data
                          )
        db.session.add(appt)
        db.session.commit()
    #if request.method == 'POST' and 'image' in request.files:
       # image = images.save(request.files['image'])
        #form.populate_obj(settings)
        #db.session.add(settings)
        #db.session.commit()
        flash('Data edited!', 'success')
        return redirect(url_for('event.events'))
    else:
        flash('Error! Data was not added.', 'error')
    return render_template('event/edit_event.html', form=form, url=url)


@event.route('/event', defaults={'page': 1}, methods=['GET'])
@event.route('/event/<int:page>', methods=['GET'])
#@login_required
#@admin_required
def events(page):
    events = Event.query.paginate(page, per_page=100)
    return render_template('event/browse.html', events=events)


@event.route('/event/<int:event_id>/_delete', methods=['GET'])
@login_required
@admin_required
def delete_event(event_id):
    event = Event.query.filter_by(id=event_id).first()
    db.session.delete(event)
    db.session.commit()
    flash('Successfully deleted a event member.', 'success')
    return redirect(url_for('event.events'))



##@event.route('/event/add', methods=['Get', 'POST'])
##@event.route('/add/event', methods=['Get', 'POST'])
###@login_required
##def add_event():
##    org = Organisation.query.get(1)
##    form = EventForm()
##
##    if request.method == 'POST' and 'image' in request.files:
##        image = images.save(request.files['image'])
##        appt = Event(image=image,
##                    #owner_organisation=org.org_name,
##                    #organisation_id=org.id,
##
##                    event_name = form.event_name.data,
##                    event_title = form.event_title.data,
##                    event_description = form.event_description.data
##                          )
##        db.session.add(appt)
##        db.session.commit()
##        flash('Event added!', 'success')
##        return redirect(url_for('event.events'))
##    return render_template('event/add_event.html', form=form, org=org)
