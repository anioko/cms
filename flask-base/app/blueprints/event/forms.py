from flask_ckeditor import CKEditorField
from flask_uploads import UploadSet, IMAGES
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    SelectField,
    IntegerField,
    DecimalField,
    FloatField,
    FileField,
    BooleanField,
    MultipleFileField,
    TextAreaField
    
)
from wtforms.fields.html5 import EmailField, DateField, DateTimeLocalField
from wtforms.validators import (
    Email,
    EqualTo,
    InputRequired,
    Length,
)
from wtforms.validators import Email, EqualTo, InputRequired, Length, Optional
from wtforms_alchemy import Unique, ModelForm, model_form_factory
from app.models import *


images = UploadSet('images', IMAGES)
BaseModelForm = model_form_factory(FlaskForm)

class EventForm(FlaskForm):
    name = StringField('Event Name E.g Sunday Service')
    title = StringField('Event Title or Slogan')
    description = TextAreaField('Event Description')
    start_time = DateTimeLocalField('StartDate & Time:', format='%Y-%m-%dT%H:%M')
    end_time = DateTimeLocalField('EndDate & Time:', format='%Y-%m-%dT%H:%M')
    video = FileField('Video', validators=[FileRequired()])
    image = FileField('Welcome banner Image to be displayed', validators=[Optional(), FileAllowed(images, 'Images only!')])
    banner_displayed = BooleanField("Display banner ?")
    login_required = BooleanField("Registration Required ?")
    submit = SubmitField('Submit')
