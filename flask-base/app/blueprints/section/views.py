import json

from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from flask_uploads import UploadSet, IMAGES
from flask_wtf.file import FileAllowed
from wtforms import Flags

from app import db
from app.blueprints.section.forms import SectionForm, ContentForm
from app.decorators import admin_required
from app.models import *
from werkzeug.utils import secure_filename
from sqlalchemy import distinct, func


from flask import Blueprint, render_template

from app.models import EditableHTML

section = Blueprint('section', __name__)
images = UploadSet('images', IMAGES)
photos = UploadSet('photos', IMAGES)


@section.app_errorhandler(500)
def internal_server_error(_):
    return render_template('errors/500.html'), 500


@section.route('/section/add', methods=['Get', 'POST'])
@section.route('/add/section', methods=['Get', 'POST'])
#@login_required
def add_section():
    org = Organisation.query.get(1)

    #section_name = db.session.query(func.count(Section.id)).\
        #group_by(Section.section_name)
    section_name = db.session.query(func.count(Section.id)).\
        group_by(Section.section_name)#.first()
    
    
    if section_name >=1 :
        flash('Section {} already exist'.format(Section.section_name), 'warning')
        return redirect(url_for('section.sections'))
    
    form = SectionForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #image_filename = ""
            #if request.files['image']:
                #image_filename = images.save(request.files['image'])
            appt = Section(#image=image_filename,
                    #owner_organisation=org.org_name,
                    #organisation_id=org.id,
                section_name = form.section_name.data,
                public = form.public.data
                           )
            db.session.add(appt)
            db.session.commit()
            flash('Section {} successfully created'.format(appt.section_name), 'success')
            return redirect(url_for('section.sections'))
    return render_template('section/add_section.html', form=form, org=org)


@section.route('/<int:section_id>/section/edit', methods=['GET', 'POST'])
#@login_required
def edit_section(section_id):
    org = Organisation.query.get(1)
    settings = Section.query.filter_by(id=section_id).first_or_404()
    photo = Section.query.filter_by(id=section_id).first_or_404()
    url = images.url(photo.image)
    form = SectionForm(obj=settings)
    if request.method == 'POST' and 'image' in request.files:
        image = images.save(request.files['image'])
        appt = Section(image=image,
                      #owner_organisation=org.org_name,
                      #organisation_id=org.id,
                    section_name = form.section_name.data
                    #section_title = form.section_title.data
                    #section_description = form.section_description.data
                          )
        db.session.add(appt)
        db.session.commit()
    #if request.method == 'POST' and 'image' in request.files:
       # image = images.save(request.files['image'])
        #form.populate_obj(settings)
        #db.session.add(settings)
        #db.session.commit()
        flash('Data edited!', 'success')
        return redirect(url_for('section.sections'))
    else:
        flash('Error! Data was not added.', 'error')
    return render_template('section/edit_section.html', form=form, url=url)


@section.route('/section', defaults={'page': 1}, methods=['GET'])
@section.route('/section/<int:page>', methods=['GET'])
#@login_required
#@admin_required
def sections(page):
    sections = Section.query.paginate(page, per_page=100)
    return render_template('section/browse.html', sections=sections)


@section.route('/section/<int:section_id>/_delete', methods=['GET'])
@login_required
@admin_required
def delete_section(section_id):
    section = Section.query.filter_by(id=section_id).first()
    db.session.delete(section)
    db.session.commit()
    flash('Successfully deleted a section member.', 'success')
    return redirect(url_for('section.sections'))


@section.route('/<int:section_id>/<section_name>/content/add', methods=['GET', 'POST'])
@login_required
@admin_required
def add_content(section_id, section_name):
    
    content_exist = Content.query.filter_by(section_id=section_id).count()
    if content_exist >= 1:
        flash('Contents for {} already exist'.format(section_name), 'warning')
        return redirect(url_for('section.add_section'))
    
    settings = Section.query.filter_by(id=section_id, section_name=section_name).first()
    
    form = ContentForm(obj=settings)
    if request.method == 'POST':
        if form.validate_on_submit():
            appt = Content(section_id=section_id)
            form.populate_obj(appt)
            db.session.add(appt)
            db.session.commit()
            flash('Contents for {} successfully created'.format(appt.section_name), 'success')
            return redirect(url_for('section.contents'))
    return render_template('section/add_content.html', form=form)


@section.route('/content', defaults={'page': 1}, methods=['GET'])
@section.route('/content/<int:page>', methods=['GET'])
#@login_required
#@admin_required
def contents(page):
    contents = Content.query.paginate(page, per_page=100)
    section = Section.query.all()
    return render_template('section/contents.html', contents=contents, section=section)

@section.route('/<int:content_id>/content/edit', methods=['GET', 'POST'])
#@login_required
def edit_content(content_id):
    settings = Content.query.filter_by(id=content_id).first_or_404()
    photo = Content.query.filter_by(id=content_id).first_or_404()
    url = images.url(photo.image)
    form = ContentForm(obj=settings)
    if request.method == 'POST' and 'image' in request.files:
        image = images.save(request.files['image'])
        appt = Content(image=image,
                      #owner_organisation=org.org_name,
                      #organisation_id=org.id,
                    sections = form.sections.data,
                    title = form.title.data,
                    video = form.video.data,
                    text = form.text.data,
                    description = form.description.data
                          )
        db.session.add(appt)
        db.session.commit()
    #if request.method == 'POST' and 'image' in request.files:
       # image = images.save(request.files['image'])
        #form.populate_obj(settings)
        #db.session.add(settings)
        #db.session.commit()
        flash('Data edited!', 'success')
        return redirect(url_for('section.contents'))
    else:
        flash('Error! Data was not added.', 'error')
    return render_template('section/edit_content.html', form=form, url=url)

@section.route('/content/<int:content_id>/_delete', methods=['GET'])
@login_required
@admin_required
def delete_content(content_id):
    content = Content.query.filter_by(id=content_id).first()
    db.session.delete(content)
    db.session.commit()
    flash('Successfully deleted a section member.', 'success')
    return redirect(url_for('section.contents'))

##@section.route('/section/add', methods=['Get', 'POST'])
##@section.route('/add/section', methods=['Get', 'POST'])
###@login_required
##def add_section():
##    org = Organisation.query.get(1)
##    form = SectionForm()
##
##    if request.method == 'POST' and 'image' in request.files:
##        image = images.save(request.files['image'])
##        appt = Section(image=image,
##                    #owner_organisation=org.org_name,
##                    #organisation_id=org.id,
##
##                    section_name = form.section_name.data,
##                    section_title = form.section_title.data,
##                    section_description = form.section_description.data
##                          )
##        db.session.add(appt)
##        db.session.commit()
##        flash('Section added!', 'success')
##        return redirect(url_for('section.sections'))
##    return render_template('section/add_section.html', form=form, org=org)
