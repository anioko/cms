from flask_ckeditor import CKEditorField
from flask_uploads import UploadSet, IMAGES
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    SelectField,
    IntegerField,
    DecimalField,
    FloatField,
    FileField,
    BooleanField,
    MultipleFileField,
    TextAreaField
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    Email,
    EqualTo,
    InputRequired,
    Length,
)
from wtforms.validators import Email, EqualTo, InputRequired, Length, Optional
from wtforms_alchemy import Unique, ModelForm, model_form_factory
from app.models import *



images = UploadSet('images', IMAGES)
BaseModelForm = model_form_factory(FlaskForm)

def get_section():
    yield from Section.query

class SectionForm(FlaskForm):
    public = SelectField(u'Make section name public on navigation bar?', choices=[('Yes', 'Yes'), ('No', 'No')])
    section_name = SelectField(u'Section Name', choices=[('jumbotron', 'Marketing Header'), ('Portfolio', 'Portfolio'),
                                                         ('Services', 'Services'), ('about us', 'About Us'),
                                                         ('Testimonials', 'Testimonials'), ('footer', 'footer'),
                                                         ('Pricing', 'Pricing'), ('call2action', 'Call to action'),
                                                         ('Team', 'Team'), ('Faq', 'Faq'),
                                                         ('Features', 'Features')])
    #section_title = StringField('Section Title E.g Portfolio or About Us or Slogan')
    #section_description = StringField('E.g Find out what our customers say about us')
    #section_video = StringField('Link to the video E.g https://youtu.be/3iIviz54lKA')
    #image = FileField('Image', validators=[Optional(), FileAllowed(images, 'Images only!')])
    submit = SubmitField('Submit')



class ContentForm(FlaskForm):
    #sections = QuerySelectMultipleField(
       # 'Section Name',
       # validators=[InputRequired()],
        #get_label='section_name',
        #query_factory=get_section)
    section_name = StringField('Section Name')
    title = StringField('Title E.g We are the best in town')
    text = StringField('Text E.g Watch Video')
    video = StringField('Link to the video E.g https://youtu.be/3iIviz54lKA')
    #image = FileField(' Image', validators=[Optional(), FileAllowed(images, 'Images only!')])
    description = TextAreaField('Description', [Optional()])
    submit = SubmitField('Submit')    
