from flask import Blueprint, render_template, abort, flash, redirect, request
from flask_login import current_user, login_required

from app.decorators import admin_required
from app.email import send_email
from .forms import *
from sqlalchemy import func
organisation = Blueprint('organisation', __name__)

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}




def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@organisation.route('/orgs', defaults={'page': 1}, methods=['GET'])
@organisation.route('/orgs/<int:page>', methods=['GET'])
@login_required
@admin_required
def orgs(page):
    orgs = Organisation.query.paginate(page, per_page=100)
    org = Organisation.query.first_or_404()
    return render_template('organisation/browse.html', orgs=orgs, org=org)

@organisation.route('/logos', defaults={'page': 1}, methods=['GET'])
@organisation.route('/logos/<int:page>', methods=['GET'])
@login_required
@admin_required
def logos(page):
    logos = Logo.query.paginate(page, per_page=100)
    logo = Logo.query.first()
    if logo is None:
        return redirect(url_for('organisation.logo_upload'))

    return render_template('organisation/logo_browse.html', logos=logos, logo=logo)


@organisation.route('/organization/<int:org_id>/edit', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_org(org_id):
    settings = Organisation.query.filter_by(id=org_id).first_or_404()
    form = OrganisationForm(obj=settings)
    if request.method == 'POST':
        if form.validate_on_submit():

            form.populate_obj(settings)
            db.session.add(settings)
            db.session.commit()
            flash('Settings successfully edited', 'success')
            check_users_exist = db.session.query(func.count(User.id)).scalar()
            if check_users_exist <= 0:
                return redirect(url_for('account.admin_register'))
            else:
                return redirect(url_for('admin.frontend_dashboard'))
    return render_template('organisation/edit_org.html', form=form, settings=settings)

@organisation.route('/org/<int:org_id>/_delete', methods=['POST'])
@login_required
@admin_required
def delete_org(org_id):
    org = Organisation.query.filter_by(id=org_id).first()
    db.session.delete(org)
    db.session.commit()
    flash('Successfully deleted Organisation.', 'success')
    return redirect(url_for('organisation.orgs'))

@organisation.route('/logo/upload', methods=['GET', 'POST'])
@login_required
def logo_upload():
    ''' check if logo already exist, if it does, send to homepage. Avoid duplicate upload here'''
    check_logo_exist = db.session.query(Logo).filter(Logo.organisation_id == Organisation.id).count()
    if check_logo_exist >= 1:
        return redirect(url_for('admin.frontend_dashboard'))
    form = LogoForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = images.save(request.files['logo'])
            image_url = images.url(image_filename)
            owner_organisation = db.session.query(Organisation).filter_by(user_id=current_user.id).first()
            logo = Logo(
                image_filename=image_filename,
                image_url=image_url,
                owner_organisation=owner_organisation.org_name,
                organisation_id=owner_organisation.id
            )
            db.session.add(logo)
            db.session.commit()
            flash("Image saved.")
            return redirect(url_for('organisation.logos'))
        else:
            flash('ERROR! Logo was not saved.', 'error')
    return render_template('organisation/upload.html', form=form)

@organisation.route('/logo/<int:logo_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_logo_upload(logo_id):
    logo = Logo.query.filter_by(id=logo_id).first_or_404()
    form = LogoForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = images.save(request.files['logo'])
            image_url = images.url(image_filename)
            owner_organisation = db.session.query(Organisation).filter_by(user_id=current_user.id).first()
            logo.image_filename=image_filename,
            logo.image_url=image_url,
            logo.owner_organisation=owner_organisation.org_name,
            logo.organisation_id=owner_organisation.id
            db.session.add(logo)
            db.session.commit()
            flash("Image saved.")
            return redirect(url_for('organisation.logos'))
        else:
            flash('ERROR! Logo was not saved.', 'error')
    return render_template('organisation/upload.html', form=form)

@organisation.route('/logo/<int:logo_id>/_delete', methods=['GET'])
@login_required
@admin_required
def delete_logo(logo_id):
    logo = Logo.query.filter_by(id=logo_id).first()
    db.session.delete(logo)
    db.session.commit()
    flash('Successfully deleted Logo.', 'success')
    return redirect(url_for('admin.frontend_dashboard'))
