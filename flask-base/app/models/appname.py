from .. import db

from datetime import datetime
from logging import log
from time import time
from sqlalchemy.ext.hybrid import hybrid_property

class AppName(db.Model):
    __tablename__ = 'appnames'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), default=None, nullable=False)
    users = db.relationship('User', backref='appname', lazy='dynamic')

    created_at = db.Column(db.DateTime, default=db.func.now())
    updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    def __repr__(self):
        return '<AppName \'%s\'>' % self.name

    @hybrid_property
    def appname(self):
        return self.name

