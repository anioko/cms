from .. import db
from datetime import datetime
from logging import log
from time import time

class Programme(db.Model):
    __tablename__ = 'programmes'
    id = db.Column(db.Integer, primary_key=True)
    segment = db.Column(db.String)
    description = db.Column(db.Text)
    start_time= db.Column(db.DateTime)
    end_time= db.Column(db.DateTime)
    banner = db.Column(db.String, default=None, nullable=True)
    banner_displayed = db.Column(db.Boolean, default=False)
    video = db.Column(db.String)
    event_id = db.Column(db.Integer, db.ForeignKey('events.id'))
    
    created_at = db.Column(db.DateTime, default=db.func.now())
    updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)
