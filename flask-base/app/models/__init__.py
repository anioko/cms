"""
These imports enable us to make all defined models members of the models
module (as opposed to just their python files)
"""

from .user import *  # noqa
from .miscellaneous import *  # noqa
from .page import *  # noqa
from .organisation import *  # noqa
from .logo import *  # noqa
from .section import *  # noqa
from .appname import *  # noqa
from .marketplace import *  # noqa
from .blog import *  # noqa
from .programme import *  # noqa
from .banner import *  # noqa
from .video import *  # noqa
from .event import *  # noqa
from .content import *  # noqa
