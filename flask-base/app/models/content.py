from .. import db
from datetime import datetime
from logging import log
from time import time
from sqlalchemy.orm import backref, query_expression
from app.models import Section


class Content(db.Model):
    __tablename__ = 'contents'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String())
    image = db.Column(db.String, default=None, nullable=True)
    video = db.Column(db.String)
    text = db.Column(db.String)
    section_name = db.Column(db.String)
    section_id = db.Column(db.Integer, db.ForeignKey('sections.id', ondelete="CASCADE"),
                           nullable=False)
    description = db.Column(db.String())
    created_at = db.Column(db.DateTime, default=db.func.now())
    updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


    def __repr__(self):
        return '<Content %r>' % self.title


