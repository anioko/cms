from .. import db
from datetime import datetime
from logging import log
from time import time

class Section(db.Model):
    __tablename__ = 'sections'
    id = db.Column(db.Integer, primary_key=True)
    section_name = db.Column(db.String)
    #section_title = db.Column(db.String)
    #section_description = db.Column(db.String)
    #section_video = db.Column(db.String)
    #image = db.Column(db.String, default=None, nullable=True)
    public = db.Column(db.String)
    contents = db.relationship('Content', backref='section', lazy='dynamic')
   
    created_at = db.Column(db.DateTime, default=db.func.now())
    updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    #def __repr__(self):
        #return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)

    def __repr__(self):
        return '<Section %r>' % self.section_name

    def get_section():
        return  Section.query
