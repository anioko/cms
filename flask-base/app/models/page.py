from .. import db
from datetime import datetime
from logging import log
from time import time


class Page(db.Model):
    __tablename__ = 'pages'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(), default=None, nullable=False)
    title = db.Column(db.String(150))
    description = db.Column(db.Text)
    html_code_one = db.Column(db.Text)
    html_code_two = db.Column(db.Text)
    html_code_three = db.Column(db.Text)
    html_code_four = db.Column(db.Text)
    google_analytics_id = db.Column(db.String(25), unique=True)
    other_tracking_analytics_one = db.Column(db.Text)
    other_tracking_analytics_two = db.Column(db.Text)
    other_tracking_analytics_three = db.Column(db.Text)
    other_tracking_analytics_four = db.Column(db.Text)
    images = db.Column(db.String(), default=None, nullable=True)
    is_onnavbar = db.Column(db.Boolean, default=False)
    is_frontpagevisible = db.Column(db.Boolean, default=False)
    is_landingpage = db.Column(db.Boolean, default=False)
    signin_required = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime, default=db.func.now())
    updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    organisation_id = db.Column(db.Integer, db.ForeignKey('organisations.id', ondelete="CASCADE"))
    owner_organisation = db.Column(db.String(128))


    @property
    def image_items(self):
        return [url_for('_uploads.uploaded_file', setname='images',
                        filename=image, _external=True) for image in json.loads(self.images)]

