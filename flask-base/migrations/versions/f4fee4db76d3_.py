"""empty message

Revision ID: f4fee4db76d3
Revises: 510a1fba4a30
Create Date: 2020-10-06 16:22:30.491801

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f4fee4db76d3'
down_revision = '510a1fba4a30'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('events',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('title', sa.String(), nullable=True),
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('start_time', sa.DateTime(), nullable=True),
    sa.Column('end_time', sa.DateTime(), nullable=True),
    sa.Column('banner', sa.String(), nullable=True),
    sa.Column('banner_displayed', sa.Boolean(), nullable=True),
    sa.Column('login_required', sa.Boolean(), nullable=True),
    sa.Column('video', sa.String(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column('programmes', sa.Column('event_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'programmes', 'events', ['event_id'], ['id'])
    op.drop_column('programmes', 'name')
    op.drop_column('programmes', 'title')
    op.drop_column('programmes', 'login_required')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('programmes', sa.Column('login_required', sa.BOOLEAN(), autoincrement=False, nullable=True))
    op.add_column('programmes', sa.Column('title', sa.VARCHAR(), autoincrement=False, nullable=True))
    op.add_column('programmes', sa.Column('name', sa.VARCHAR(), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'programmes', type_='foreignkey')
    op.drop_column('programmes', 'event_id')
    op.drop_table('events')
    # ### end Alembic commands ###
