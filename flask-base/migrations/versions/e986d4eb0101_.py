"""empty message

Revision ID: e986d4eb0101
Revises: 399b907eb2c5
Create Date: 2020-10-04 21:21:52.499908

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e986d4eb0101'
down_revision = '399b907eb2c5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('banners',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('image_filename', sa.String(), nullable=True),
    sa.Column('image_url', sa.String(), nullable=True),
    sa.Column('organisation_id', sa.Integer(), nullable=False),
    sa.Column('owner_organisation', sa.String(length=128), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['organisation_id'], ['organisations.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('banners')
    # ### end Alembic commands ###
